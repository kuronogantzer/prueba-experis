# Proyecto de árbol binario

El proyecto es una aplicación back-end, en concreto una API REST hecha en `python` que se puede ejecutar con las siguientes instrucciones:

1. Instalar una versión de Python superior a 3.5 ([descarga e instalación](https://www.python.org/downloads/))
2. Instalar el gestor de paquetes de python PIP ([descarga e instalación](https://pip.pypa.io/en/stable/installing/))
3. Una vez clonado el proyecto e instaladas las herramientas ingresar a la consola de comandos, navegar a la carpeta **Back-end** y ejecutar `pip install -r requeriments.txt`
4. Al terminar la instalación de las dependencias se puede ejecutar la aplicación con el comando `python API/BinaryTreeAPI.py`
5. Para la ejecución de las pruebas unitarias debe estar en ejecución la aplicación y luego correr el comando `python -m unittest UnitTests`

---

## BinaryTreeAPI

Los end points expuestos a través de la API son los siguientes
 
#### URL base: http://localhost:5000
---
- ### Cargar árbol binario a partir de archivo .csv

    **End point**: /load_binary_tree
    
    **Método**: POST
    
    **Parámetros**: Archivo en formato .csv
    
---
- ### Consultar el árbol binario

    **End point**: /binary_tree
    
    **Método**: GET
    
    **Parámetros**: Ninguno
    
---
- ### Agregar un nodo al árbol binario

    **End point**: /binary_tree
    
    **Método**: POST
    
    **Parámetros**: Objeto en formato JSON en el `body` de la petición
    
    **Ejemplo**: `{ 'value': 50, 'parent': 70 }`
    
---
- ### Eliminar un nodo del árbol binario

    **End point**: /binary_tree
    
    **Método**: DELETE
    
    **Parámetros**: Objeto en formato JSON en el `body` de la petición
    
    **Ejemplo**: `{ 'value': 50, 'force_delete': true }`
    
--- 
- ### Buscar el ancestro común de dos nodos

    **End point**: /binary_tree/<value_a>/<value_b>
    
    **Método**: GET
    
    **Parámetros**: el valor de los nodos a y b en la URL del end point
    
    **Ejemplo**: `http://localhost:5000/binary_tree/48/80`