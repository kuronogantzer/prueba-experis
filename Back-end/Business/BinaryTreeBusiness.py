from Entities.BinaryTree import BinaryTree
import csv


class BinaryTreeBusiness:

    def __init__(self):
        self.binary_tree = BinaryTree()

    def create_tree(self, csv_file):
        '''
        Crea un árbol binario a partir de un archivo con formato csv cuyo separador sea ","
        :param csv_file: Objeto que contiene el archivo csv cargado por el usuario
        :return: Retorna el primer nodo del árbol construido a partir del archivo
        '''
        try:
            # Se intenta leer el archivo como un csv que usar como separador el caracter ","
            try:
                csv_reader = csv.reader(csv_file, delimiter=",")
            except:
                raise FileExistsError("El archivo no existe o no tiene el formato CSV adecuado.")

            # Se recorre cada línea del archivo
            for row in csv_reader:
                if len(row) > 0:
                    previous_node = None
                    for node in row:
                        previous_node = self.binary_tree.add_node(parent=previous_node.value if previous_node else None,
                                                                  value=int(node))
                        previous_node = previous_node if not isinstance(previous_node, tuple) else previous_node[0]
            return self.binary_tree.get_first_node()
        except Exception as e:
            raise e

    def ancestor(self, value_a: int, value_b: int):
        '''
        Busca el ancestro común de dos nodos
        :param value_a: valor del nodo A
        :param value_b: valor del nodo B
        :return: Si los dos nodos existen dentro del árbol binario, retorna el nodo que sea ancestro común de los dos.
        '''
        try:
            # Se busca el nodo A y su nodo padre
            node_a, parent_node = self.binary_tree.search_node(value_a, with_parent=True)
            ancestor = parent_node
            node_b = None
            ignored_node = None

            # Si se encontro el nodo A comienza a buscar el nodo B de forma ascendente a partir de los nodos padres del
            # nodo A
            if node_a:

                # Se repite la búsqueda hasta encontrar el nodo B
                while node_b is None:
                    node_b = self.binary_tree.search_node(value_b, ancestor, ignored_node=ignored_node)

                    # Si el nodo B no se encuentra entre los hijos del ancestro actual, se busca el nodo del ancestro
                    # actual para obtener su nodo padre y establecerlo como el nuevo ancestro
                    if node_b is None:
                        ignored_node, ancestor = self.binary_tree.search_node(ancestor.value, with_parent=True)
                    else:
                        break

            # Si se encontraron los dos nodos se retorna el ancestro común, de lo contrario se retorna None
            return ancestor if node_a and node_b else None
        except Exception as e:
            print(e)
            return None, e

    def add_node(self, value, parent=None):
        try:
            return self.binary_tree.add_node(value, parent)
        except Exception as e:
            raise Exception("Ha ocurrido un problema al agregar el nodo: " + str(e))

    def delte_node(self, value: int, force_delete: bool = False):
        try:
            return self.binary_tree.delete_node(value, force_delete)
        except Exception as e:
            raise Exception("Ha ocurrido un problema al eliminar el nodo: " + str(e))

    def get_first_node(self):
        try:
            return self.binary_tree.get_first_node()
        except Exception as e:
            raise Exception("Ha ocurrido un problema al intentar obtener el árbol binario: " + str(e))
