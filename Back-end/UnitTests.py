import unittest
import json
from Entities.BinaryTree import BinaryTree
from Business.BinaryTreeBusiness import BinaryTreeBusiness
from API.BinaryTreeAPI import app
from io import StringIO, BytesIO


class TestBinaryTree(unittest.TestCase):

    # PRUEBAS UNITARIAS DE LA ENTIDAD

    def test_add_node(self):
        binary_tree = BinaryTree()
        node = binary_tree.add_node(value=70, parent=None)
        self.assertEqual(node.value, 70)
        son_node = binary_tree.add_node(parent=node.value, value=50)
        self.assertEqual(son_node.value, 50)

    def test_delete_node(self):
        binary_tree = BinaryTree()
        node = binary_tree.add_node(value=50, parent=None)
        self.assertEqual(node.value, 50)
        success = binary_tree.delete_node(node.value)
        self.assertEqual(success, True)

    def test_search_node(self):
        binary_tree = BinaryTree()
        node = binary_tree.add_node(value=70, parent=None)
        binary_tree.add_node(value=50, parent=node.value)
        found_node = binary_tree.search_node(50)
        self.assertEqual(found_node.value, 50)
        found_node = binary_tree.search_node(30)
        self.assertEqual(found_node, None)

    def test_get_first_node(self):
        binary_tree = BinaryTree()
        binary_tree.add_node(55, None)
        binary_tree.add_node(30, 55)
        binary_tree.add_node(40, 55)
        node = binary_tree.get_first_node()
        self.assertEqual(node.value, 55)

    # PRUEBAS UNITARIAS LA CAPA DE NEGOCIO

    def test_create_binary_tree(self):
        bt_business = BinaryTreeBusiness()
        file_tree = open("BinaryTree.csv", "r")
        first_node = bt_business.create_tree(file_tree)
        file_tree.close()
        self.assertEqual(first_node.value, 70)

    def test_ancestor(self):
        bt_business = BinaryTreeBusiness()
        node = bt_business.add_node(value=70, parent=None)
        son_node = bt_business.add_node(value=50, parent=node.value)
        bt_business.add_node(value=40, parent=son_node.value)
        bt_business.add_node(value=20, parent=son_node.value)
        bt_business.add_node(value=30, parent=node.value)
        ancestor = bt_business.ancestor(value_a=40, value_b=30)
        self.assertEqual(ancestor.value, 70)

    def test_delete_node_business(self):
        bt_business = BinaryTreeBusiness()
        node = bt_business.add_node(value=35, parent=None)
        success = bt_business.delte_node(node.value)
        self.assertEqual(success, True)

    def test_get_first_node(self):
        bt_business = BinaryTreeBusiness()
        bt_business.add_node(40, None)
        bt_business.add_node(30, 40)
        bt_business.add_node(20, 30)
        first_node = bt_business.get_first_node()
        self.assertEqual(first_node.value, 40)

    # PRUEBAS UNITARIAS DE LA API

    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True

    def test_add_get_binary_tree_EP(self):
        self.test_delete_binary_tree()
        headers = {'content-type': 'application/json'}
        data = json.dumps({'value': 50})
        self.app.post('/binary_tree', headers=headers, data=data)
        result = self.app.get('/binary_tree')
        self.assertEqual(result.status_code, 200)
        self.assertEqual(json.loads(result.get_data(as_text=True))['value'], 50)


    def test_delete_node_EP(self):
        self.test_delete_binary_tree()
        headers = {'content-type': 'application/json'}
        data = json.dumps({'value': 50})
        self.app.post('/binary_tree', headers=headers, data=data)
        result = self.app.delete('/binary_tree', headers=headers, data=data)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(json.loads(result.get_data(as_text=True)), True)

    def test_load_binary_tree_EP(self):
        self.test_delete_binary_tree()
        file = open("BinaryTree.csv", "r")
        content = file.read()
        data = dict(file=(BytesIO(bytes(content, 'utf-8')), 'BinaryTree.txt'))
        result = self.app.post('/load_binary_tree', buffered=True, content_type='multipart/form-data', data=data)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(json.loads(result.get_data(as_text=True))['value'], 70)

    def test_ancestor_EP(self):
        self.test_delete_binary_tree()
        headers = {'content-type': 'application/json'}
        data = json.dumps({'value': 90})
        self.app.post('/binary_tree', headers=headers, data=data)
        data = json.dumps({'value': 80, 'parent': 90})
        self.app.post('/binary_tree', headers=headers, data=data)
        data = json.dumps({'value': 70, 'parent': 90})
        self.app.post('/binary_tree', headers=headers, data=data)
        data = json.dumps({'value': 60, 'parent': 70})
        self.app.post('/binary_tree', headers=headers, data=data)
        result = self.app.get('/binary_tree/60/80')
        self.assertEqual(json.loads(result.get_data(as_text=True))['value'], 90)

    def test_delete_binary_tree(self):
        headers = {'content-type': 'application/json'}
        result = self.app.get('/binary_tree')
        data_response = json.loads(result.get_data(as_text=True))
        if data_response:
            node_value = data_response['value']
            data = json.dumps({'value': node_value, 'force_delete': True})
            self.app.delete('/binary_tree', headers=headers, data=data)


if __name__ == '__main__':
    unittest.main()
