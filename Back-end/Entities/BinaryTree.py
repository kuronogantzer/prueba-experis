from json import JSONEncoder


class Node(JSONEncoder):

    value: int
    childs: list
    skipkeys = False
    ensure_ascii = True
    check_circular = True
    allow_nan = True
    sort_keys = False
    indent = None
    separators = None
    default = None

    def __init__(self, value=None):
        self.value = value
        self.childs = None

    def default(self, o):
        return o.__dict__


class BinaryTree:

    __first_node: Node = None

    def __init__(self, first_node=None):
        self.__first_node = first_node

    def get_first_node(self):
        return self.__first_node

    def add_node(self, value: int, parent: int):
        try:
            parent_node = self.search_node(parent)

            # Crea un nuevo objeto Nodo si al buscarlo en el árbol no existe, de lo contrario lo deja vacío.
            node_exists = self.search_node(value, self.__first_node)
            node = Node(value) if node_exists is None else None
            if node is None:
                return node_exists, ValueError('Ya existe el nodo')

            if parent_node and parent_node.childs and len(parent_node.childs) > 1:
                raise ValueError('El nodo ' + str(parent) + ' ya tiene 2 nodos hijos: ' + str(parent_node.childs[0].value) +
                                 ', ' + str(parent_node.childs[1].value))


            if self.__first_node is None:
                self.__first_node = node
            elif parent is None:
                raise ValueError('No se pueden crear dos nodos raíz para el árbol binario ni sobreescribir el actual.')
            else:
                if parent_node:
                    if parent_node.childs is None:
                        parent_node.childs = list()
                    parent_node.childs.append(node)

            return node
        except Exception as e:
            print(e)
            raise e

    def delete_node(self, value: int, force_delete=False):
        '''
        Elimina un nodo del árbol binario
        :param value: valor del nodo que se desea eliminar
        :param force_delete: permite eliminar un nodo cuando tiene hijos
        :return: Retorna True si la eliminación fue exitosa, de lo contrario retorna False
        '''
        try:
            # Se busca el nodo junto a su nodo padre
            result = self.search_node(value, with_parent=True)
            parent_node = None
            node = None

            # Si el resultado es de tipo Nodo, no tiene padres, de lo contrario viene una tupla, con el nodo y su padre
            if isinstance(result, Node):
                node = result
            else:
                node, parent_node = result

            # Se procede a eliminar el nodo si no tiene hijos y si tiene entonces solo si se fuerza el borrado.
            if node and (node.childs and force_delete) or node.childs is None:
                    if parent_node is not None:
                        childs = [child_node for child_node in parent_node.childs
                                              if child_node.value is not value]
                        parent_node.childs = childs if len(childs) > 0 else None
                    else:
                        self.__first_node = None
            else:
                raise Exception("No se puede eliminar un nodo que tiene hijos")
            return True
        except Exception as e:
            print(e)
            raise e

    def search_node(self, value: int, current_node: Node = None, with_parent: bool = False, ignored_node: Node = None):
        '''
        Función que busca un nodo a paratir del nodo indicado, si no se indica un nodo actual la búsqueda inicia
        por el primer nodo del árbol
        :param value: valor del nodo buscado
        :param current_node: nodo a partir del cual se inicia la búsqueda
        :param with_parent: si se desea retornar el nodo junto con su nodo padre
        :param ignored_node: nodo que no será tenido en cuenta en la búsqueda
        :return: Si el parámetro with_parent es True retorna una TUPLA donde el primer elemento es el nodo buscado
        y el segundo el nodo padre, de lo contrario retorna únicamente el nodo buscado
        '''
        try:
            # Si no hay nodo actual para la búsqueda inicia desde el primer nodo del árbol
            current_node = self.__first_node if current_node is None else current_node

            # Si el nodo actual es el nodo ignorado no sigue la búsqueda por este nodo
            if ignored_node and current_node.value is ignored_node.value:
                return None

            # Si el nodo actual tiene el valor buscado, lo retorna
            if current_node and value is current_node.value:
                return current_node

            # Si el nodo actual no es el buscado, busca entre sus hijos
            elif current_node and current_node.childs and len(current_node.childs) > 0:
                node = None
                parent_node = None
                for child in current_node.childs:

                    # Si ya encontro el nodo, no realiza mas busquedas recursivas
                    node = node if node else self.search_node(value, child, with_parent=True, ignored_node=ignored_node)

                    # Si el resultado de la busqueda no es un simple Nodo, es una tupla que viene con su nodo padre
                    if not isinstance(node, Node) and node is not None:
                        node, parent_node = node

                # Si se solicitó retornar el nodo padre se retorna una tupla donde el padre es el segundo elemento
                if with_parent:
                    parent_node = current_node if parent_node is None else parent_node
                    return node, None if node is None else parent_node
                else:
                    return node
            else:
                # El nodo actual no es el buscado y ya se buscó en todos sus hijos sin éxito
                return None
        except Exception as e:
            print(e)
            return False, e
