from flask import Flask, jsonify, request
from flask_cors import CORS, cross_origin
from Business.BinaryTreeBusiness import BinaryTreeBusiness
from Entities.BinaryTree import Node
import os

app = Flask(__name__)

cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

bt_business = BinaryTreeBusiness()


@app.route('/load_binary_tree', methods=['POST'])
@cross_origin()
def load_binary_tree():
    try:
        first_node = None
        data_tree = request.files['file']
        content = data_tree.read()
        if data_tree and content:
            if not os.path.exists("trees/"):
                os.makedirs("trees/")
            file = open("trees/binary_tree.csv", "wb")
            file.write(content)
            file.close()
            file = open("trees/binary_tree.csv", "r")
            first_node = bt_business.create_tree(file)
            file.close()
        return Node().encode(first_node), 200
    except Exception as e:
        return jsonify(e), 500


@app.route("/binary_tree", methods=['GET'])
@cross_origin()
def get_binary_tree():
    try:
        tree = bt_business.get_first_node()
        return Node().encode(tree), 200
    except Exception as e:
        return jsonify(e), 500


@app.route("/binary_tree", methods=['POST'])
@cross_origin()
def add_new_node():
    try:
        data = request.get_json()
        required = ['value']
        if not all(k in data for k in required):
            return 'Datos incompletos', 500

        node = bt_business.add_node(data['value'], data['parent'] if 'parent' in data.keys() else None)
        return Node().encode(node), 200
    except Exception as e:
        return jsonify(e), 500


@app.route("/binary_tree", methods=['DELETE'])
@cross_origin()
def delete_node():
    try:
        data: dict = request.get_json()
        required = ['value']
        if not all(k in data for k in required):
            return 'Datos incompletos', 500

        success = bt_business.delte_node(data['value'], data['force_delete'] if 'force_delete' in data.keys() else None)
        return jsonify(success), 200
    except Exception as e:
        return jsonify(e), 500

@app.route("/binary_tree/<int:value_a>/<int:value_b>", methods=["GET"])
@cross_origin()
def ancestor(value_a: int, value_b: int):
    try:
        ancestor = bt_business.ancestor(value_a, value_b)
        return Node().encode(ancestor), 200
    except Exception as e:
        return jsonify(e), 500


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
